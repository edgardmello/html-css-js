
//scroll animation
function reveal() {
    var reveals = document.querySelectorAll(".reveal");
    for (var i = 0; i < reveals.length; i++) {
        var windowHeight = window.innerHeight;
        var elementTop = reveals[i].getBoundingClientRect().top;
        var elementVisible = 150;

        if (elementTop < windowHeight - elementVisible) {
            reveals[i].classList.add("active");
        }
    }
}

window.addEventListener("scroll", reveal);

//btn slide
function handleNextSlide() {

}



//check-box price
function handleCheckbox() {
    let checkbox = document.getElementById("toggle")
    let money = document.getElementById("money")
    if (checkbox.checked === true) {
        let price = 29;
        let tmp = setInterval(() => {
            price++;
            money.innerText = price
            if (price === 49)
                clearInterval(tmp)
        }, 20)
    }
    else {
        let price = 49;
        let tmp = setInterval(() => {
            price--;
            money.innerText = price
            if (price === 29)
                clearInterval(tmp)
        }, 20)
    }
}

function showMenu() {
    let menu = document.getElementById("menu")
    let body = document.getElementById("body")
    menu.style.display = "block"
    body.style.overflow = "hidden"
}

function closeMenu() {
    let menu = document.getElementById("menu")
    let body = document.getElementById("body")
    menu.style.display = "none"
    body.style.overflow = "scroll"
}




// auto-typing
var TxtType = function (el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function () {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function () {
        that.tick();
    }, delta);
};

window.onload = function () {
    var elements = document.getElementsByClassName('typewrite');
    for (var i = 0; i < elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
            new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);

    //business satify
    percent = document.getElementById("satifi-percent")
    sales = document.getElementById("total-sale")
    hours = document.getElementById("hours")
    day = document.getElementById("day")
    let number = 1
    let tmp = setInterval(() => {
        number += 1;
        sales.innerText = number
        percent.innerText = number;
        if (number <= 24)
            hours.innerText = number
        if (number <= 7)
            day.innerText = number
        if (number === 100)
            clearInterval(tmp)
    }, 10)

};

//validate form
const inputsForm = document.querySelectorAll(".form__custom-input>input");
const formButton = document.querySelector(".form__btn-download");
inputsForm.forEach((el) => {
    el.addEventListener("keyup", (e) => {
        el.classList.remove("invalid");
        if (e.keyCode === 13) {
            e.preventDefault();
            formButton.click();
        }
    });
});
formButton.addEventListener("click", (e) => {
    e.preventDefault();
    const [name, email, password] = inputsForm;
    const errors = [];
    if (name.value.length === 0) {
        errors.push("name is not valid");
        name.classList.add("invalid");
    } else name.classList.remove("invalid");
    const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (!re.test(email.value)) {
        errors.push("email is not valid");
        email.classList.add("invalid");
    } else email.classList.remove("invalid");
    if (password.value.length < 8) {
        errors.push("password is not valid");
        password.classList.add("invalid");
    } else password.classList.remove("invalid");

    if (errors.length === 0) console.log(name.value, email.value, password.value);
});


//change slide
var btnNext = document.getElementById("btn-next")
var btnPre = document.getElementById("btn-pre")

function changeSlide(typeBtn) {
    img1 = document.getElementById("slide-img")
    img2 = document.getElementById("slide-img1")
    content1 = document.getElementById("content")
    content2 = document.getElementById("content1")
    content1.classList.remove(`${typeBtn}-slide`)
    content2.classList.remove(`${typeBtn}-slide`)
    content1.classList.add(`${typeBtn}-slide`)
    content2.classList.add(`${typeBtn}-slide`)
    if (img1.style.display === "none") {
        img2.style.display = "none";
        content2.style.display = "none"
        img1.style.display = "block";
        content1.style.display = "block"
        content1.classList.add("active")
    }
    else {
        img1.style.display = "none";
        content1.style.display = "none"
        img2.style.display = "block";
        content2.style.display = "block"
        content2.classList.add("active")
    }

}


var menuAccount = document.getElementsByClassName("menu__account-item")
for (let i = 0; i < menuAccount.length; i++)
    menuAccount[i].addEventListener("click", () => {
        let listClass = [...menuAccount[i].classList]
        if (listClass.includes("active"))
            menuAccount[i].classList.remove("active")
        else
            if (!listClass.includes("active")) {
                for (let j = 0; j < menuAccount.length; j++) {
                    let list = [...menuAccount[j].classList]
                    if (list.includes("active"))
                        menuAccount[j].classList.remove("active")
                }
                menuAccount[i].classList.add("active")
            }

    })